ACTOR DP51Ammo : Ammo
{
	Inventory.Amount 36
	Inventory.MaxAmount 72
	Ammo.BackpackAmount 36
	Ammo.BackpackMaxAmount 144
}

ACTOR MP5Ammo : Ammo
{
	Inventory.Amount 30
	Inventory.MaxAmount 60
	Ammo.BackpackAmount 30
	Ammo.BackpackMaxAmount 60
}