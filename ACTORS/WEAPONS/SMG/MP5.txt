/* SINGLE */

ACTOR MP5 : Weapon {
	Weapon.SlotNumber 3
	Weapon.SelectionOrder 500
	Weapon.SlotPriority 2
	Weapon.AmmoType1 "MP5Ammo"
	Weapon.AmmoGive1 15
	Weapon.AmmoUse1 1
	Weapon.AmmoType2 "MP5Mag"
	Weapon.AmmoUse2 1
	Weapon.BobStyle "Smooth"
	Weapon.BobSpeed 2.5
	Weapon.BobRangeX 0.70
	Weapon.BobRangeY 0.70
	//Weapon.UpSound "weapons/riotgun/raise"
	Inventory.Pickupmessage "You got the MP5!"
	//Inventory.PickupSound "weapons/rifle/up"
	Obituary "%o was gunned down by %k's Rifle."
	+WEAPON.NOALERT
	+WEAPON.AMMO_OPTIONAL
	+NOAUTOAIM
	Tag "MP5"
	Scale 1.0
	States {
		Ready:
			TNT1 A 0 A_JumpIfInventory("MP5Mag", 0, "RealReady")
			TNT1 A 0 A_TakeInventory("MP5Ammo", 1, TIF_NOTAKEINFINITE)
			TNT1 A 0 A_GiveInventory("MP5Mag", 1)
			Loop
		RealReady:
			TNT1 A 0 A_JumpIfInventory("MP5Count", 2, "LowerWeapon")
			MP5S A 1 A_WeaponReady
			Loop
		Select:
			TNT1 A 0 A_JumpIfInventory("MP5Count", 2, "DualTime")
			MP5S A 2 Offset(-106, 137)
			MP5S A 1 Offset(-92, 123)
			//MP5S A 1 Offset(-79, 110)
			MP5S A 1 Offset(-67, 98)
			//MP5S A 1 Offset(-56, 87)
			MP5S A 1 Offset(-45, 77)
			//MP5S A 1 Offset(-36, 68)
			MP5S A 1 Offset(-28, 60)
			//MP5S A 1 Offset(-21, 53)
			MP5S A 1 Offset(-15, 47)
			//MP5S A 1 Offset(-10, 42)
			MP5S A 1 Offset(-6, 38)
			//MP5S A 1 Offset(-3, 35)
			MP5S A 1 Offset(-1, 33)
			MP5S A 1 A_Raise
			Loop
		Deselect:
			MP5S A 1 A_Lower
			Loop
		Fire:
			TNT1 A 0 A_JumpIfInventory("MP5Mag", 1, 1)
			Goto ReloadWeap
			TNT1 A 0 A_PlaySound("weapons/MP5/fire", 1)
			TNT1 A 0 A_FireProjectile("MP5Tracer", random(0,1), 0, 10, -5, 0, random(-1,1))
			TNT1 A 0 A_TakeInventory("MP5Mag", 1)
			MP5F A 1 Offset(2, 36)
			MP5F B 1 Offset(1, 35) A_FireCustomMissile("GunSmokeSpawner",0,0,8,0)
			MP5F C 1 Offset(0, 34)
			TNT1 A 0 A_Refire
			Goto RealReady
		ReloadWeap:
			TNT1 A 0 A_JumpIfNoAmmo("ThrowOut")
			MP5S A 1 Offset(0, 35)
			MP5S A 1 Offset(0, 39)
			MP5S A 1 Offset(0, 45)
			MP5S A 1 Offset(0, 57)
		ReloadLoop:
			TNT1 A 0 A_TakeInventory("MP5Ammo", 1, TIF_NOTAKEINFINITE)
			TNT1 A 0 A_GiveInventory("MP5Mag", 1)
			TNT1 A 0 A_JumpIfInventory("MP5Mag", 60, "ReloadEnd")
			TNT1 A 0 A_JumpIfInventory("MP5Ammo", 1, "ReloadLoop")
			Goto ReloadEnd
		ReloadEnd:
			MP5S A 1 Offset(0, 57)
			MP5S A 1 Offset(0, 45)
			MP5S A 1 Offset(0, 39)
			MP5S A 1 Offset(0, 35)
			Goto RealReady
		LowerWeapon:
			MP5S A 1 Offset(-1, 33)
			MP5S A 1 Offset(-3, 35)
			MP5S A 1 Offset(-6, 38)
			MP5S A 1 Offset(-10, 42)
			MP5S A 1 Offset(-15, 47)
			MP5S A 1 Offset(-21, 53)
			MP5S A 1 Offset(-28, 60)
			MP5S A 1 Offset(-36, 68)
			MP5S A 1 Offset(-45, 77)
			MP5S A 1 Offset(-56, 87)
			MP5S A 1 Offset(-67, 98)
			MP5S A 1 Offset(-79, 110)
			MP5S A 1 Offset(-92, 123)
			MP5S A 2 Offset(-106, 137)
		DualTime:
			TNT1 A 0 A_TakeInventory("MP5")
			TNT1 A 0 A_GiveInventory("MP5Dual")
			TNT1 A 0 A_GiveInventory("MP5DualMag", 60)
			TNT1 A 0 A_SelectWeapon("MP5Dual")
			Stop
		ThrowOut:
			MP5S A 1 Offset(0, 35)
			MP5S A 1 Offset(0, 45)
			MP5S A 1 Offset(0, 58)
			MP5S A 1 Offset(0, 69)
			MP5S A 1 Offset(0, 81)
			MP5S A 1 Offset(0, 95)
			MP5S A 1 Offset(0, 115)
			TNT1 A 0 A_TakeInventory("MP5Count", 1)
			TNT1 A 0 A_TakeInventory("MP5Pickup", 1)
			TNT1 A 0 A_TakeInventory("MP5", 1)
			Stop
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_SpawnItemEx("MP5Pickup", 0, 0, 0, 0, 0, 0, 0, 0)
			MP5X A -1
			Stop
	}
}

ACTOR MP5Mag : Ammo {
	+IGNORESKILL
	Inventory.MaxAmount 30
}

/* DUAL */

ACTOR MP5Dual : Weapon {
	Weapon.SlotNumber 3
	Weapon.SelectionOrder 400
	Weapon.SlotPriority 1
	Weapon.AmmoType1 "MP5Ammo"
	Weapon.AmmoGive1 15
	Weapon.AmmoUse1 1
	Weapon.AmmoType2 "MP5DualMag"
	Weapon.AmmoUse2 1
	Weapon.BobStyle "Smooth"
	Weapon.BobSpeed 2.5
	Weapon.BobRangeX 0.70
	Weapon.BobRangeY 0.70
	//Weapon.UpSound "weapons/riotgun/raise"
	Inventory.Pickupmessage "You got the MP51's!"
	//Inventory.PickupSound "weapons/rifle/up"
	Obituary "%o was gunned down by %k's Rifle."
	+WEAPON.NOALERT
	+WEAPON.AMMO_OPTIONAL
	+NOAUTOAIM
	Tag "MP5Dual"
	Scale 1.0
	States {
		Ready:
			MP5D A 1 A_WeaponReady
			Loop
		Select:
			TNT1 A 0 A_GiveInventory("MP5DualSelected", 1)
			MP5R M 1 Offset(1, 123)
			//MP5R L 1 Offset(1, 110)
			MP5R K 1 Offset(1, 98)
			//MP5R J 1 Offset(1, 87)
			MP5R I 1 Offset(1, 77)
			//MP5R H 1 Offset(1, 68)
			MP5R G 1 Offset(1, 60)
			//MP5R F 1 Offset(1, 53)
			MP5R E 1 Offset(1, 47)
			//MP5R D 1 Offset(1, 42)
			MP5R C 1 Offset(1, 38)
			//MP5R B 1 Offset(1, 35)
			MP5R A 1 Offset(1, 33)
			MP5D A 1 A_Raise
			Loop
		Deselect:
			MP5D A 1 A_Lower
			Loop
		Fire:
			TNT1 A 0 A_JumpIfInventory("MP5DualMag", 1, 1)
			Goto ReloadWeap
			TNT1 A 0 A_PlaySound("weapons/MP5/fire", 1)
			TNT1 A 0 A_FireProjectile("MP5Tracer", random(0,1), 0, 10, -5, 0, random(-1,1))
			TNT1 A 0 A_FireProjectile("MP5Tracer", random(0,1), 0, -10, -5, 0, random(-1,1))
			TNT1 A 0 A_TakeInventory("MP5DualMag", 2)
			MPDF A 1 Offset(2, 36)
			TNT1 A 0 A_FireCustomMissile("GunSmokeSpawner",0,0,8,0)
			MPDF B 1 Offset(1, 35) A_FireCustomMissile("GunSmokeSpawner",0,0,8,0)
			MPDF C 1 Offset(0, 34)
			TNT1 A 0 A_Refire
			Goto Ready
		ReloadWeap:
			TNT1 A 0 A_JumpIfNoAmmo("ThrowOut")
			MP5D A 1 Offset(0, 35)
			MP5D A 1 Offset(0, 39)
			MP5D A 1 Offset(0, 45)
			MP5D A 1 Offset(0, 57)
		ReloadLoop:
			TNT1 A 0 A_TakeInventory("MP5Ammo", 1, TIF_NOTAKEINFINITE)
			TNT1 A 0 A_GiveInventory("MP5DualMag", 1)
			TNT1 A 0 A_JumpIfInventory("MP5DualMag", 60, "ReloadEnd")
			TNT1 A 0 A_JumpIfInventory("MP5Ammo", 1, "ReloadLoop")
			Goto ReloadEnd
		ReloadEnd:
			MP5S A 1 Offset(0, 57)
			MP5S A 1 Offset(0, 45)
			MP5S A 1 Offset(0, 39)
			MP5S A 1 Offset(0, 35)
			Goto Ready
		ThrowOut:
			MP5D A 1 Offset(0, 35)
			MP5D A 1 Offset(0, 41)
			MP5D A 1 Offset(0, 50)
			MP5D A 1 Offset(0, 55)
			MP5D A 1 Offset(0, 61)
			TNT1 A 0 A_TakeInventory("MP5Count", 2)
			TNT1 A 0 A_TakeInventory("MP5Pickup", 2)
			TNT1 A 0 A_TakeInventory("MP5Dual", 1)
			Stop
	}
}

ACTOR MP5DualMag : Ammo {
	+IGNORESKILL
	Inventory.MaxAmount 60
}

ACTOR MP5Count : Inventory {
	Inventory.MaxAmount 2
}

ACTOR MP5DualSelected : Inventory {
	Inventory.MaxAmount 1
}

ACTOR MP5Empty {
	Radius 6
	Height 6
	Speed 36
	Damage 3
	+MISSILE
	+DONTFALL
	+NOEXTREMEDEATH
	+EXPLODEONWATER
	BounceFactor 0.3
	WallBounceFactor 0.2
	BounceType "Doom"
	BounceSound "GBOUNCE"
	-BOUNCEONACTORS
	+ALLOWBOUNCEONACTORS
	Gravity 0.7
	Mass 5
	Alpha 1.0
	States {
		Spawn:
			MP5X A -1
			Stop
		Death:
			MP5X AAAAAAAAAA 2 A_FadeOut(0.1)
			Stop
	}
}