ACTOR GunSmokeSpawner
{
	Speed 20
	+NOCLIP
	States {
		Spawn:
			TNT1 A 1
			TNT1 A 0 Thing_ChangeTID(0,390)
			TNT1 AA 0 A_SpawnProjectile ("GunSmoke", 0, 0, random (0, 360), 2, random (0, 180))
			Stop
	}
}

ACTOR GunSmoke
{
	+NOGRAVITY
	+NOBLOCKMAP
	+FLOORCLIP
	+FORCEXYBILLBOARD
	+NOINTERACTION
	+FORCEXYBILLBOARD
	+MISSILE
	Speed 1
	RenderStyle Add
	Alpha 0.06
	Radius 0
	Height 0
	Scale 0.6
	States {
		Spawn:
			TNT1 A 1
			SMKE AABBCCDDEEFFGGHHIIJJKK 1 A_FadeOut(0.001)
			Stop
	}
}