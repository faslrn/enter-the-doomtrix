#include ACTORS/WEAPONS/SMG/MP5.txt

#include ACTORS/WEAPONS/AMMO.txt
#include ACTORS/WEAPONS/PICKUPS.txt

/* PLAYER */
#include ACTORS/PLAYER/PLAYER.txt

/* SFX */
#include ACTORS/SFX/SMOKE.txt

/* LOGIC */
#include ACTORS/LOGIC/MARKERS.txt