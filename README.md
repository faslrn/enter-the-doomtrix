Thread does not exist on GZDoom forums as of yet!
=================================================================================

This GIT houses the development of Enter the Doomtrix, a GZDOOM mod. You will see 
three branches:

- Development: Development of newer changes for either testing or feedback
- Release: Stable release
- Master: Base code in case I screw something up

If you have any questions, bug reports, feature requests, etc.; post in the
main thread or add an issue/feature request here in GitLab.

For any new changes, please check out the development branch and check for any
new pushes!
